import Vue from 'vue'
import Router from 'vue-router'
import Settings from '@/components/Settings'
import Home from '@/components/Home'
import Gps from '@/components/Gps'

Vue.use(Router)


const router = new Router({
   mode: 'hash',
   base: '/',
   routes: [
    {
        path:'/settings',
        name: 'settings',
        component: Settings
      },
      {
        path:'/',
        name: 'home',
        component: Home
      },
      {
       path: '/gps',
       name: 'geolocation',
       component: Gps,
    }
   ]
})

export default router
