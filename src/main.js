import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import PouchDB from 'pouchdb'
PouchDB.plugin(require('pouchdb-upsert'));
const axios = require('axios');
Vue.config.productionTip = false;

Vue.use(Vuetify, {
  iconfont: 'mdi'
});

Vue.prototype.$http = axios;
Vue.prototype.$settings = new PouchDB('settings');


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
